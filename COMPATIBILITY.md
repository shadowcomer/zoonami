# Zoonami Compatibility

Zoonami relies on item groups, node groups, and biomes for core functionality. The problem is that there are no set standards for these. Games, mods, and mapgen can choose completely different names, groups, etc.

Generally, Minetest the Game, MineClone 2, and other games based on these two games are the "safest" options when it comes to a balanced gameplay experience. However, a more detailed explanation on what is compatible is provided below.

## Supported Games
The following is a list of games that are known to be compatible with Zoonami:
* Minetest the Game (MtG) - Fully compatible. Zoonami is developed and tested on this game the most.
* MineClone 2 - Fully compatible, but it receives less testing.
* Farlands Reloaded - Should be compatible, but it receives very little testing.
* Hades Revisted - Technically compatible if using a mod that adds snow to the game. Without snow, some monsters can't spawn and can never be found. Many monsters also have spawning conditions that won't be found naturally. Players will need to build artificial habitats to get these monsters to spawn.
* Exile - Technically compatible, but it doesn't have a lot of synergy with Zoonami. Zoonami adds in a crafting grid page for its own recipes, but it does not add in a crafting guide.

## Unsupported Games
The following is a list of games that are known to be incompatible with Zoonami:
* Nodecore - Completely changes crafting recipes to not take place in a crafting grid.

## Mapgen
At the time of writing this, Minetest has 8 different mapgens:
* v5, v7, valleys, carpathian are the recommended mapgens.
* Fractal and flat are compatible, but might not provide the best experience.
* v6 will not generate villages due how it handles biomes. It's playable, but not recommended.
* Singlenode might technically work if playing a skyblock type game. It's not recommended.

Each mapgen also has different flags for fine tuning how it generates. For the best experience, you'll want to enable caves, mountains, hills, etc when possible as some monsters only spawn underground and some only spawn on top of hills and mountains.

## Item Groups
Items groups are used in crafting recipes to avoid having to list specific items. Unfortunately, there is no standard item group list that games and mods use. Currently there is support for Minetest the Game, MineClone 2, Farlands Reloaded, Hades Revisted, and Exile. Games based off of these should also be compatible as long as the item groups remain the same.

## Node Groups
Nodes groups are used for mob spawns and mapgen decoration placement. Unfortunately, there is no standard node group list that games and mods use. Currently there is support for Minetest the Game, MineClone 2, Farlands Reloaded, Hades Revisted, and Exile. Games based off of these should also be compatible as long as the node groups remain the same.

## Biomes
Biomes are used to determine where villages and berry bushes spawn. If a biome isn't listed in Zoonami, it means either the biome was unsuitable for villages and berry bushes or support has not been added yet. Villages can only spawn in biomes with little to no trees. If no supported biomes are found, Zoonami will still be playable except that villages won't generate and berry bushes will generate in any biome. The current games supported include Minetest the Game, MineClone 2, Farlands Reloaded, and Exile. The current mods supported include Ethereal, Loria, Extra Biomes, MoreBiomes, Wildflower Fields, Redwood Biome, Wilhelmines Natural Biomes, Wilhelmines Living Jungle, Everness, 30 Biomes, Saltd, and Swampz.
