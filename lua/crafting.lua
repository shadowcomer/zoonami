-- Import files
local mod_path = minetest.get_modpath("zoonami")
local fs = dofile(mod_path .. "/lua/formspec.lua")
local group = dofile(mod_path .. "/lua/group.lua")

-- Add crafting grid for compatible games that don't provide one
if minetest.get_modpath("exile_env_sounds") and minetest.get_modpath("sfinv") then
	sfinv.register_page("zoonami:crafting", {
		title = "Zoonami Crafting",
		get = function(self, player, context)
			local formspec = fs.image(4.75, 1.5, 1, 1, "sfinv_crafting_arrow.png", 1)..
				fs.list("current_player", "craft", 1.75, 0.5, 3, 3, 0, 1)..
				fs.list("current_player", "craftpreview", 5.75, 1.5, 1, 1, 0, 1)..
				fs.listring("current_player", "main")..
				fs.listring("current_player", "craft")
				
			return sfinv.make_formspec(player, context, formspec, true)
		end
	})
end

-- Jelly
local function register_jelly(asset_name)
	minetest.register_craft({
		output = "zoonami:basic_"..asset_name.."_jelly 1",
		recipe = {
			{"zoonami:"..asset_name.."_berry", "zoonami:"..asset_name.."_berry"},
			{"zoonami:"..asset_name.."_berry", "zoonami:"..asset_name.."_berry"},
		}
	})

	minetest.register_craft({
		output = "zoonami:improved_"..asset_name.."_jelly 1",
		recipe = {
			{"zoonami:basic_"..asset_name.."_jelly", "zoonami:basic_"..asset_name.."_jelly"},
			{"zoonami:basic_"..asset_name.."_jelly", "zoonami:basic_"..asset_name.."_jelly"},
		}
	})

	minetest.register_craft({
		output = "zoonami:advanced_"..asset_name.."_jelly 1",
		recipe = {
			{"zoonami:improved_"..asset_name.."_jelly", "zoonami:improved_"..asset_name.."_jelly"},
			{"zoonami:improved_"..asset_name.."_jelly", "zoonami:improved_"..asset_name.."_jelly"},
		}
	})
end

-- Jelly
register_jelly("blue")
register_jelly("red")
register_jelly("orange")
register_jelly("green")

-- Candy
local function register_candy(asset_name)
    minetest.register_craft({
        output = "zoonami:"..asset_name.."_candy 1",
        recipe = {
            {"", "zoonami:improved_"..asset_name.."_jelly", ""},
            {"zoonami:improved_"..asset_name.."_jelly", "zoonami:"..asset_name.."_berry", "zoonami:improved_"..asset_name.."_jelly"},
            {"", "zoonami:improved_"..asset_name.."_jelly", ""},
        }
    })
end

register_candy("blue")
register_candy("red")
register_candy("orange")
register_candy("green")

-- Sanded Plank
minetest.register_craft({
	output = "zoonami:sanded_plank 4",
	recipe = {
		{group.sand, group.wood},
	}
})

-- Stick
minetest.register_craft({
	output = "zoonami:stick 2",
	recipe = {
		{"zoonami:sanded_plank"},
	}
})

-- Plank Floor
minetest.register_craft({
	output = "zoonami:plank_floor 1",
	recipe = {
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
	}
})

-- Cube Floor
minetest.register_craft({
	output = "zoonami:cube_floor 2",
	recipe = {
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
	}
})

-- Bookshelf
minetest.register_craft({
	output = "zoonami:bookshelf 1",
	recipe = {
		{group.wood, "zoonami:sanded_plank", group.wood},
		{"group:book", "group:book", "group:book"},
		{group.wood, "zoonami:sanded_plank", group.wood},
	}
})

-- Countertop
minetest.register_craft({
	output = "zoonami:countertop 1",
	recipe = {
		{group.stone, group.stone},
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
	}
})

-- Countertop Cabinet
minetest.register_craft({
	output = "zoonami:countertop_cabinet 1",
	recipe = {
		{"zoonami:countertop"},
	}
})

-- Countertop Drawers
minetest.register_craft({
	output = "zoonami:countertop_drawers 1",
	recipe = {
		{"zoonami:countertop_cabinet"},
	}
})

-- Countertop (Crafting Loop)
minetest.register_craft({
	output = "zoonami:countertop 1",
	recipe = {
		{"zoonami:countertop_drawers"},
	}
})

-- Light
minetest.register_craft({
	output = "zoonami:light 2",
	recipe = {
		{"zoonami:zeenite_ingot"},
		{"zoonami:crystal_glass"},
	}
})

-- Blue Rug
minetest.register_craft({
	output = "zoonami:blue_rug 2",
	recipe = {
		{"zoonami:cloth", "zoonami:blue_tulip", "zoonami:cloth"},
	}
})

-- Yellow Rug
minetest.register_craft({
	output = "zoonami:yellow_rug 2",
	recipe = {
		{"zoonami:cloth", "zoonami:sunflower", "zoonami:cloth"},
	}
})

-- Orange Rug
minetest.register_craft({
	output = "zoonami:orange_rug 2",
	recipe = {
		{"zoonami:cloth", "zoonami:tiger_lily", "zoonami:cloth"},
	}
})

-- Wood Table
minetest.register_craft({
	output = "zoonami:wood_table 1",
	recipe = {
		{"zoonami:sanded_plank", "zoonami:sanded_plank", "zoonami:sanded_plank"},
		{"group:stick", "", "group:stick"},
		{"group:stick", "", "group:stick"},
	}
})

-- Wood Chair
minetest.register_craft({
	output = "zoonami:wood_chair 1",
	recipe = {
		{"zoonami:sanded_plank", "", ""},
		{"zoonami:sanded_plank", "zoonami:sanded_plank", "zoonami:sanded_plank"},
		{"group:stick", "", "group:stick"},
	}
})

-- NPC Wood Chair
minetest.register_craft({
	type = "shapeless",
	output = "zoonami:npc_wood_chair_active 1",
	recipe = {"zoonami:wood_chair", "zoonami:100_zc_coin"}
})

-- Classic Door
minetest.register_craft({
	output = "zoonami:classic_door 1",
	recipe = {
		{"group:stick", "group:stick"},
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
	}
})

-- Gravel Path
minetest.register_craft({
	output = "zoonami:gravel_path 4",
	recipe = {
		{group.sand, group.sand},
		{group.stone, group.stone},
	}
})

-- Dirt Path
minetest.register_craft({
	output = "zoonami:dirt_path 4",
	recipe = {
		{group.sand, group.sand},
		{group.soil, group.soil},
	}
})

-- White Brick
minetest.register_craft({
	output = "zoonami:white_brick 8",
	recipe = {
		{group.stone, group.stone, group.stone},
		{group.stone, "zoonami:daisy", group.stone},
		{group.stone, group.stone, group.stone},
	}
})

-- Tile Nodes
local function tile_node_craft(asset_name, flower_name)
	minetest.register_craft({
		output = "zoonami:"..asset_name.." 4",
		recipe = {
			{"", group.stone, ""},
			{group.stone, flower_name, group.stone},
			{"", group.stone, ""},
		}
	})
end

-- Tile Nodes
tile_node_craft("blue_tile", "zoonami:blue_tulip")
tile_node_craft("yellow_tile", "zoonami:sunflower")
tile_node_craft("orange_tile", "zoonami:tiger_lily")
tile_node_craft("red_tile", "zoonami:zinnia")
tile_node_craft("white_tile", "zoonami:daisy")

-- Blue Roof
minetest.register_craft({
	output = "zoonami:blue_roof 8",
	recipe = {
		{group.stone, group.stone, group.stone},
		{group.stone, "zoonami:blue_tulip", group.stone},
		{group.stone, group.stone, group.stone},
	}
})

-- Blue Roof Stairs
minetest.register_craft({
	output = "zoonami:blue_roof_stairs 3",
	recipe = {
		{"", "zoonami:blue_roof"},
		{"zoonami:blue_roof", "zoonami:blue_roof"},
	}
})

-- Red Roof
minetest.register_craft({
	output = "zoonami:red_roof 8",
	recipe = {
		{group.stone, group.stone, group.stone},
		{group.stone, "zoonami:zinnia", group.stone},
		{group.stone, group.stone, group.stone},
	}
})

-- Red Roof Stairs
minetest.register_craft({
	output = "zoonami:red_roof_stairs 3",
	recipe = {
		{"", "zoonami:red_roof"},
		{"zoonami:red_roof", "zoonami:red_roof"},
	}
})

-- Crystal Glass
minetest.register_craft({
	type = "cooking",
	output = "zoonami:crystal_glass",
	recipe = "zoonami:crystal_wall",
	cooktime = 3,
})

-- Window
minetest.register_craft({
	output = "zoonami:window 8",
	recipe = {
		{"zoonami:crystal_glass", "zoonami:crystal_glass"},
		{"zoonami:crystal_glass", "zoonami:crystal_glass"},
	}
})

-- Door
minetest.register_craft({
	output = "zoonami:door 1",
	recipe = {
		{"zoonami:window", "zoonami:window"},
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
		{"zoonami:sanded_plank", "zoonami:sanded_plank"},
	}
})

-- Cloth
minetest.register_craft({
	type = "shapeless",
	output = "zoonami:cloth 1",
	recipe = {"group:stick", "group:wool", "group:stick"}
})

-- Paper
minetest.register_craft({
	output = "zoonami:paper 1",
	recipe = {
		{group.sand, "zoonami:sanded_plank"},
	}
})

-- Pictures
local function picture_craft(output, input)
	minetest.register_craft({
		output = output,
		recipe = input and {{input}} or {
			{"group:stick", "group:stick", "group:stick"},
			{"group:stick", "zoonami:paper", "group:stick"},
			{"group:stick", "group:stick", "group:stick"},
		}
	})
end

-- Pictures
picture_craft("zoonami:beach_picture 1")
picture_craft("zoonami:flower_picture 1", "zoonami:beach_picture")
picture_craft("zoonami:island_picture 1", "zoonami:flower_picture")
picture_craft("zoonami:sailboat_picture 1", "zoonami:island_picture")
picture_craft("zoonami:springtime_picture 1", "zoonami:sailboat_picture")
picture_craft("zoonami:starfish_picture 1", "zoonami:springtime_picture")
picture_craft("zoonami:tree_picture 1", "zoonami:starfish_picture")
picture_craft("zoonami:beach_picture 1", "zoonami:tree_picture")

-- Guide Book
minetest.register_craft({
	output = "zoonami:guide_book 1",
	recipe = {
		{"zoonami:paper"},
		{"zoonami:paper"},
		{"zoonami:paper"},
	}
})

-- Monster Journal
minetest.register_craft({
	output = "zoonami:monster_journal 1",
	recipe = {
		{"zoonami:guide_book"}
	}
})

-- Move Journal
minetest.register_craft({
	output = "zoonami:move_journal 1",
	recipe = {
		{"zoonami:monster_journal"}
	}
})

-- Guide Book (Crafting Loop)
minetest.register_craft({
	output = "zoonami:guide_book 1",
	recipe = {
		{"zoonami:move_journal"}
	}
})

-- Backpack
minetest.register_craft({
	output = "zoonami:backpack 1",
	recipe = {
		{"zoonami:cloth", "zoonami:cloth", "zoonami:cloth"},
		{"zoonami:cloth", "", "zoonami:cloth"},
		{"zoonami:cloth", "zoonami:cloth", "zoonami:cloth"},
	}
})

-- Zeenite Ingot
minetest.register_craft({
	type = "cooking",
	output = "zoonami:zeenite_ingot",
	recipe = "zoonami:zeenite_lump",
})

-- Zeenite Ingot
minetest.register_craft({
	output = "zoonami:zeenite_ingot 9",
	recipe = {
		{"zoonami:zeenite_block"}
	}
})

-- Zeenite Block
minetest.register_craft({
	output = "zoonami:zeenite_block 1",
	recipe = {
		{"zoonami:zeenite_ingot", "zoonami:zeenite_ingot", "zoonami:zeenite_ingot"},
		{"zoonami:zeenite_ingot", "zoonami:zeenite_ingot", "zoonami:zeenite_ingot"},
		{"zoonami:zeenite_ingot", "zoonami:zeenite_ingot", "zoonami:zeenite_ingot"},
	}
})

-- Empty Pail
minetest.register_craft({
	output = "zoonami:pail_empty",
	recipe = {
		{"zoonami:zeenite_ingot", "", "zoonami:zeenite_ingot"},
		{"", "zoonami:zeenite_ingot", ""},
	}
})

-- Monster Repellent
minetest.register_craft({
	output = "zoonami:monster_repellent",
	recipe = {
		{"group:stick"},
		{"zoonami:zeenite_ingot"},
		{"zoonami:zeenite_ingot"},
	}
})

-- Healer
minetest.register_craft({
	output = "zoonami:healer 1",
	recipe = {
		{"zoonami:window", "zoonami:window", "zoonami:window"},
		{"zoonami:crystal_fragment", "zoonami:crystal_fragment", "zoonami:crystal_fragment"},
		{"zoonami:zeenite_ingot", "zoonami:zeenite_ingot", "zoonami:zeenite_ingot"},
	}
})

-- Computer
minetest.register_craft({
	output = "zoonami:computer 1",
	recipe = {
		{"zoonami:zeenite_ingot", "zoonami:zeenite_ingot", "zoonami:zeenite_ingot"},
		{"zoonami:zeenite_ingot", "zoonami:window", "zoonami:zeenite_ingot"},
		{"zoonami:crystal_fragment", "zoonami:crystal_fragment", "zoonami:crystal_fragment"},
	}
})

-- Trading Machine
minetest.register_craft({
	output = "zoonami:trading_machine 1",
	recipe = {
		{"zoonami:computer", "zoonami:zeenite_block", "zoonami:computer"},
	}
})

-- Vending Machine
minetest.register_craft({
	output = "zoonami:vending_machine 1",
	recipe = {
		{"zoonami:zeenite_ingot", "zoonami:zeenite_ingot", "zoonami:zeenite_ingot"},
		{"zoonami:zeenite_ingot", "zoonami:window", "zoonami:computer"},
		{"zoonami:zeenite_ingot", "group:door", "zoonami:zeenite_ingot"},
	}
})

-- Prism
minetest.register_craft({
	output = "zoonami:prism 1",
	recipe = {
		{"", "zoonami:crystal_glass", ""},
		{"zoonami:crystal_glass", "zoonami:crystal_glass", "zoonami:crystal_glass"},
		{"", "zoonami:crystal_glass", ""},
	}
})

-- 1000 ZC to 100 ZC
minetest.register_craft({
	output = "zoonami:100_zc_coin 10",
	recipe = {
		{"zoonami:1000_zc_coin"},
	}
})

-- 100 ZC to 10 ZC
minetest.register_craft({
	output = "zoonami:10_zc_coin 10",
	recipe = {
		{"zoonami:100_zc_coin"},
	}
})

-- 10 ZC to 1 ZC
minetest.register_craft({
	output = "zoonami:1_zc_coin 10",
	recipe = {
		{"zoonami:10_zc_coin"},
	}
})
