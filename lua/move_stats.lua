-- Contains all the moves that monsters can use

--Local namespace
local move_stats = {}

-- Generate Tooltip
function move_stats.tooltip(move_name)
	local move = move_stats[move_name]
	if move.category == "Recover Move" and move.type == "Health" then
		return move.category.."\nType: "..move.type.."\nAmount: "..move.amount * (100).."%\nEnergy: "..move.energy.."\nPriority: "..move.priority
	elseif move.category == "Shield Move" then
		return move.category.."\nProtection: "..(move.protection * 100).."%\nEnergy: "..move.energy.."\nPriority: "..move.priority
	elseif move.category == "Recover Move" and move.type == "Energy" then
		return move.category.."\nType: "..move.type.."\nAmount: "..move.amount.."\nEnergy: "..move.energy.."\nPriority: "..move.priority
	elseif move.category == "Counter Move" then
		return move.category.."\nType: "..move.type.."\nRange: "..move.min_power * (100).."% - "..move.max_power * (100).."%\nEnergy: "..move.energy.."\nPriority: "..move.priority
	else
		return move.category.."\nType: "..move.type.."\nAttack: "..move.power * (100).."%\nEnergy: "..move.energy.."\nPriority: "..move.priority
	end
end

--- Recover Moves ---

-- Refresh
move_stats.refresh = {
	name = "Refresh",
	asset_name = "refresh",
	category = "Recover Move",
	type = "Health",
	amount = 0.3,
	energy = 0,
	priority = 0,
	animation_frames = 3,
	frame_length = 190,
	sound = "zoonami_gear_shift",
	volume = 1
}

-- Restore
move_stats.restore = {
	name = "Restore",
	asset_name = "restore",
	category = "Recover Move",
	type = "Health",
	amount = 0.6,
	energy = 0,
	priority = -1,
	animation_frames = 3,
	frame_length = 190,
	sound = "zoonami_gear_shift",
	volume = 1
}

-- Rest
move_stats.rest = {
	name = "Rest",
	asset_name = "rest",
	category = "Recover Move",
	type = "Energy",
	amount = 2,
	energy = 0,
	priority = 0,
	animation_frames = 3,
	frame_length = 190,
	sound = "zoonami_gear_shift",
	volume = 1
}

--- Shield Moves ---

-- Guard
move_stats.guard = {
	name = "Guard",
	asset_name = "guard",
	category = "Shield Move",
	protection = 1.0,
	energy = 1,
	priority = 2,
	animation_frames = 4,
	frame_length = 250,
	sound = "zoonami_guard",
	volume = 1
}

-- Barrier
move_stats.barrier = {
	name = "Barrier",
	asset_name = "barrier",
	category = "Shield Move",
	protection = 1.0,
	energy = 0,
	priority = 1,
	animation_frames = 2,
	frame_length = 160,
	sound = "zoonami_guard",
	volume = 1
}

-- Force Field
move_stats.force_field = {
	name = "Force Field",
	asset_name = "force_field",
	category = "Shield Move",
	protection = 0.90,
	energy = 0,
	priority = 2,
	animation_frames = 2,
	frame_length = 160,
	sound = "zoonami_guard",
	volume = 1
}

--- Static Moves ---

-- Diced
move_stats.diced = {
	name = "Diced",
	asset_name = "diced",
	category = "Static Move",
	type = "Percentage",
	power = 0.2,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 190,
	sound = "zoonami_diced",
	volume = 1
}

-- Split
move_stats.split = {
	name = "Split",
	asset_name = "split",
	category = "Static Move",
	type = "Percentage",
	power = 0.25,
	energy = 3,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_illusion",
	volume = 1
}

-- Void
move_stats.void = {
	name = "Void",
	asset_name = "void",
	category = "Static Move",
	type = "Percentage",
	power = 0.34,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_shadow_orb",
	volume = 0.6
}

--- Counter Moves ---

-- Rage
move_stats.rage = {
	name = "Rage",
	asset_name = "rage",
	category = "Counter Move",
	type = "Beast",
	min_power = 1.3,
	max_power = 1.95,
	energy = 4,
	priority = 0,
	animation_frames = 2,
	frame_length = 200,
	sound = "zoonami_razor_fang",
	volume = 1
}

-- Inferno
move_stats.inferno = {
	name = "Inferno",
	asset_name = "inferno",
	category = "Counter Move",
	type = "Fire",
	min_power = 1.3,
	max_power = 1.95,
	energy = 4,
	priority = 0,
	animation_frames = 3,
	frame_length = 150,
	sound = "zoonami_man_melter",
	volume = 1
}

-- Slice
move_stats.slice = {
	name = "Slice",
	asset_name = "slice",
	category = "Counter Move",
	type = "Insect",
	min_power = 1.25,
	max_power = 2.0,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_slash",
	volume = 1
}

-- Swirl
move_stats.swirl = {
	name = "Swirl",
	asset_name = "swirl",
	category = "Counter Move",
	type = "Aquatic",
	min_power = 0.6,
	max_power = 1.7,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 160,
	sound = "zoonami_swirl",
	volume = 1
}

-- Whirlwind
move_stats.whirlwind = {
	name = "Whirlwind",
	asset_name = "whirlwind",
	category = "Counter Move",
	type = "Avian",
	min_power = 0.9,
	max_power = 1.85,
	energy = 3,
	priority = 0,
	animation_frames = 3,
	frame_length = 160,
	sound = "zoonami_gust",
	volume = 1
}

-- Thorns
move_stats.thorns = {
	name = "Thorns",
	asset_name = "thorns",
	category = "Counter Move",
	type = "Plant",
	min_power = 1.25,
	max_power = 2.0,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_pincer",
	volume = 1
}

--- Blitz Moves ---

-- Boil
move_stats.boil = {
	name = "Boil",
	asset_name = "boil",
	category = "Blitz Move",
	type = "Aquatic",
	power = 3.1,
	energy = 4,
	priority = 1,
	animation_frames = 4,
	frame_length = 160,
	sound = "zoonami_boil",
	volume = 1
}

-- Flash Fire
move_stats.flash_fire = {
	name = "Flash Fire",
	asset_name = "flash_fire",
	category = "Blitz Move",
	type = "Fire",
	power = 2.0,
	energy = 2,
	priority = 1,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_fireball",
	volume = 1
}

-- Ground Pound
move_stats.ground_pound = {
	name = "Ground Pound",
	asset_name = "ground_pound",
	category = "Blitz Move",
	type = "Rock",
	power = 2.5,
	energy = 3,
	priority = 1,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_punch",
	volume = 1
}

-- Pinpoint
move_stats.pinpoint = {
	name = "Pinpoint",
	asset_name = "pinpoint",
	category = "Blitz Move",
	type = "Plant",
	power = 2.5,
	energy = 3,
	priority = 1,
	animation_frames = 2,
	frame_length = 250,
	sound = "zoonami_pierce",
	volume = 1
}

-- Toxin
move_stats.toxin = {
	name = "Toxin",
	asset_name = "toxin",
	category = "Blitz Move",
	type = "Mutant",
	power = 2.0,
	energy = 2,
	priority = 1,
	animation_frames = 3,
	frame_length = 125,
	sound = "zoonami_poison",
	volume = 1
}

-- Ultrasonic
move_stats.ultrasonic = {
	name = "Ultrasonic",
	asset_name = "ultrasonic",
	category = "Blitz Move",
	type = "Robotic",
	power = 2.0,
	energy = 2,
	priority = 1,
	animation_frames = 3,
	frame_length = 100,
	sound = "zoonami_illusion",
	volume = 1
}

--- Basic Moves ---

-- Skip
move_stats.skip = {
	name = "Skip",
	asset_name = "skip",
	category = "Basic Move",
	type = "skip",
	power = 0,
	energy = 0,
	priority = 0,
	animation_frames = 0,
	frame_length = 0,
	sound = "zoonami_skip",
	volume = 1
}

-- Aquatic
move_stats.bubble_stream = {
	name = "Bubble Stream",
	asset_name = "bubble_stream",
	category = "Basic Move",
	type = "Aquatic",
	power = 0.9,
	energy = 2,
	priority = 0,
	animation_frames = 6,
	frame_length = 150,
	sound = "zoonami_bubble_stream",
	volume = 1
}

move_stats.high_tide = {
	name = "High Tide",
	asset_name = "high_tide",
	category = "Basic Move",
	type = "Aquatic",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 450,
	sound = "zoonami_high_tide",
	volume = 0.8
}

move_stats.aqua_jet = {
	name = "Aqua Jet",
	asset_name = "aqua_jet",
	category = "Basic Move",
	type = "Aquatic",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 100,
	sound = "zoonami_high_tide",
	volume = 0.8
}

move_stats.geyser = {
	name = "Geyser",
	asset_name = "geyser",
	category = "Basic Move",
	type = "Aquatic",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_high_tide",
	volume = 0.8
}

move_stats.vortex = {
	name = "Vortex",
	asset_name = "vortex",
	category = "Basic Move",
	type = "Aquatic",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 100,
	sound = "zoonami_spore_storm",
	volume = 1
}

move_stats.downpour = {
	name = "Downpour",
	asset_name = "downpour",
	category = "Basic Move",
	type = "Aquatic",
	power = 1.45,
	energy = 4,
	priority = 1,
	animation_frames = 3,
	frame_length = 150,
	sound = "zoonami_downpour",
	volume = 0.5
}

-- Avian
move_stats.gust = {
	name = "Gust",
	asset_name = "gust",
	category = "Basic Move",
	type = "Avian",
	power = 0.9,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 190,
	sound = "zoonami_gust",
	volume = 1
}

move_stats.swoop = {
	name = "Swoop",
	asset_name = "swoop",
	category = "Basic Move",
	type = "Avian",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_swoop",
	volume = 1
}

move_stats.peck = {
	name = "Peck",
	asset_name = "peck",
	category = "Basic Move",
	type = "Avian",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 160,
	sound = "zoonami_peck",
	volume = 1
}

move_stats.quill_drill = {
	name = "Quill Drill",
	asset_name = "quill_drill",
	category = "Basic Move",
	type = "Avian",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 3,
	frame_length = 160,
	sound = "zoonami_quill_drill",
	volume = 1
}

move_stats.twister = {
	name = "Twister",
	asset_name = "twister",
	category = "Basic Move",
	type = "Avian",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_spore_storm",
	volume = 1
}

-- Beast
move_stats.roar = {
	name = "Roar",
	asset_name = "roar",
	category = "Basic Move",
	type = "Beast",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 200,
	sound = "zoonami_roar",
	volume = 0.8
}

move_stats.pierce = {
	name = "Pierce",
	asset_name = "pierce",
	category = "Basic Move",
	type = "Beast",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 170,
	sound = "zoonami_pierce",
	volume = 1
}

move_stats.stomp = {
	name = "Stomp",
	asset_name = "stomp",
	category = "Basic Move",
	type = "Beast",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 235,
	sound = "zoonami_stomp",
	volume = 1
}

move_stats.slash = {
	name = "Slash",
	asset_name = "slash",
	category = "Basic Move",
	type = "Beast",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.bulldoze = {
	name = "Bulldoze",
	asset_name = "bulldoze",
	category = "Basic Move",
	type = "Beast",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 3,
	frame_length = 150,
	sound = "zoonami_bulldoze",
	volume = 0.6
}

-- Fire
move_stats.embers = {
	name = "Embers",
	asset_name = "embers",
	category = "Basic Move",
	type = "Fire",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 160,
	sound = "zoonami_embers",
	volume = 1
}

move_stats.scorch = {
	name = "Scorch",
	asset_name = "scorch",
	category = "Basic Move",
	type = "Fire",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 150,
	sound = "zoonami_scorch",
	volume = 0.6
}

move_stats.fireball = {
	name = "Fireball",
	asset_name = "fireball",
	category = "Basic Move",
	type = "Fire",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_fireball",
	volume = 1
}

move_stats.burnout = {
	name = "Burnout",
	asset_name = "burnout",
	category = "Basic Move",
	type = "Fire",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 160,
	sound = "zoonami_scorch",
	volume = 0.6
}

move_stats.man_melter = {
	name = "Man Melter",
	asset_name = "man_melter",
	category = "Basic Move",
	type = "Fire",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 2,
	frame_length = 150,
	sound = "zoonami_man_melter",
	volume = 0.8
}

move_stats.afterburn = {
	name = "Afterburn",
	asset_name = "afterburn",
	category = "Basic Move",
	type = "Fire",
	power = 1.25,
	energy = 2,
	priority = -1,
	animation_frames = 3,
	frame_length = 200,
	sound = "zoonami_embers",
	volume = 1.0
}

-- Insect
move_stats.pincer = {
	name = "Pincer",
	asset_name = "pincer",
	category = "Basic Move",
	type = "Insect",
	power = 0.9,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 250,
	sound = "zoonami_pincer",
	volume = 1
}

move_stats.poison_sting = {
	name = "Poison Sting",
	asset_name = "poison_sting",
	category = "Basic Move",
	type = "Insect",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_thissle_missle",
	volume = 0.7
}

move_stats.infestation = {
	name = "Infestation",
	asset_name = "infestation",
	category = "Basic Move",
	type = "Insect",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 100,
	sound = "zoonami_infestation",
	volume = 0.8
}

move_stats.life_drain = {
	name = "Life Drain",
	asset_name = "life_drain",
	category = "Basic Move",
	type = "Insect",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_life_drain",
	volume = 1
}

move_stats.bug_bite = {
	name = "Bug Bite",
	asset_name = "bug_bite",
	category = "Basic Move",
	type = "Insect",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_bug_bite",
	volume = 1
}

-- Mutant
move_stats.illusion = {
	name = "Illusion",
	asset_name = "illusion",
	category = "Basic Move",
	type = "Mutant",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_illusion",
	volume = 1.0
}

move_stats.smog = {
	name = "Smog",
	asset_name = "smog",
	category = "Basic Move",
	type = "Mutant",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_smog",
	volume = 0.8
}

move_stats.acid_bath = {
	name = "Acid Bath",
	asset_name = "acid_bath",
	category = "Basic Move",
	type = "Mutant",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 300,
	sound = "zoonami_smog",
	volume = 0.8
}

move_stats.shadow_orb = {
	name = "Shadow Orb",
	asset_name = "shadow_orb",
	category = "Basic Move",
	type = "Mutant",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_shadow_orb",
	volume = 0.6
}

move_stats.nightmare = {
	name = "Nightmare",
	asset_name = "nightmare",
	category = "Basic Move",
	type = "Mutant",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 2,
	frame_length = 150,
	sound = "zoonami_shadow_orb",
	volume = 0.6
}

-- Plant
move_stats.prickle = {
	name = "Prickle",
	asset_name = "prickle",
	category = "Basic Move",
	type = "Plant",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.vine_wrap = {
	name = "Vine Wrap",
	asset_name = "vine_wrap",
	category = "Basic Move",
	type = "Plant",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 250,
	sound = "zoonami_pincer",
	volume = 1
}

move_stats.spore_storm = {
	name = "Spore Storm",
	asset_name = "spore_storm",
	category = "Basic Move",
	type = "Plant",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 125,
	sound = "zoonami_spore_storm",
	volume = 1
}

move_stats.grass_blade = {
	name = "Grass Blade",
	asset_name = "grass_blade",
	category = "Basic Move",
	type = "Plant",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.thissle_missle = {
	name = "Thissle Missle",
	asset_name = "thissle_missle",
	category = "Basic Move",
	type = "Plant",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 5,
	frame_length = 125,
	sound = "zoonami_thissle_missle",
	volume = 0.7
}

move_stats.snare = {
	name = "Snare",
	asset_name = "snare",
	category = "Basic Move",
	type = "Plant",
	power = 1.25,
	energy = 2,
	priority = -1,
	animation_frames = 2,
	frame_length = 300,
	sound = "zoonami_pincer",
	volume = 1
}

-- Reptile
move_stats.constrict = {
	name = "Constrict",
	asset_name = "constrict",
	category = "Basic Move",
	type = "Reptile",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 250,
	sound = "zoonami_pincer",
	volume = 1
}

move_stats.tail_swipe = {
	name = "Tail Swipe",
	asset_name = "tail_swipe",
	category = "Basic Move",
	type = "Reptile",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 150,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.poison = {
	name = "Poison",
	asset_name = "poison",
	category = "Basic Move",
	type = "Reptile",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_poison",
	volume = 1
}

move_stats.spikes = {
	name = "Spikes",
	asset_name = "spikes",
	category = "Basic Move",
	type = "Reptile",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 130,
	sound = "zoonami_thissle_missle",
	volume = 0.7
}

move_stats.venom_fangs = {
	name = "Venom Fangs",
	asset_name = "venom_fangs",
	category = "Basic Move",
	type = "Reptile",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_bite",
	volume = 1
}

-- Robotic
move_stats.electrocute = {
	name = "Electrocute",
	asset_name = "electrocute",
	category = "Basic Move",
	type = "Robotic",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 100,
	sound = "zoonami_electrocute",
	volume = 1
}

move_stats.crusher = {
	name = "Crusher",
	asset_name = "crusher",
	category = "Basic Move",
	type = "Robotic",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 150,
	sound = "zoonami_crusher",
	volume = 1
}

move_stats.vice_grip = {
	name = "Vice Grip",
	asset_name = "vice_grip",
	category = "Basic Move",
	type = "Robotic",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 200,
	sound = "zoonami_crusher",
	volume = 1
}

move_stats.power_surge = {
	name = "Power Surge",
	asset_name = "power_surge",
	category = "Basic Move",
	type = "Robotic",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 3,
	frame_length = 100,
	sound = "zoonami_electrocute",
	volume = 1
}

move_stats.laser_beam = {
	name = "Laser Beam",
	asset_name = "laser_beam",
	category = "Basic Move",
	type = "Robotic",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 2,
	frame_length = 75,
	sound = "zoonami_laser_beam",
	volume = 0.4
}

move_stats.iron_fist = {
	name = "Iron Fist",
	asset_name = "iron_fist",
	category = "Basic Move",
	type = "Robotic",
	power = 1.75,
	energy = 4,
	priority = -1,
	animation_frames = 6,
	frame_length = 100,
	sound = "zoonami_punch",
	volume = 1
}

move_stats.gear_shift = {
	name = "Gear Shift",
	asset_name = "gear_shift",
	category = "Basic Move",
	type = "Robotic",
	power = 1.45,
	energy = 4,
	priority = 1,
	animation_frames = 3,
	frame_length = 70,
	sound = "zoonami_gear_shift",
	volume = 0.8
}

-- Rock
move_stats.pellet = {
	name = "Pellet",
	asset_name = "pellet",
	category = "Basic Move",
	type = "Rock",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 200,
	sound = "zoonami_pellet",
	volume = 1
}

move_stats.boulder_roll = {
	name = "Boulder Roll",
	asset_name = "boulder_roll",
	category = "Basic Move",
	type = "Rock",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 170,
	sound = "zoonami_boulder_roll",
	volume = 1
}

move_stats.rockburst = {
	name = "Rockburst",
	asset_name = "rockburst",
	category = "Basic Move",
	type = "Rock",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 200,
	sound = "zoonami_rockburst",
	volume = 1
}

move_stats.mudslide = {
	name = "Mudslide",
	asset_name = "mudslide",
	category = "Basic Move",
	type = "Rock",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_stomp",
	volume = 1
}

move_stats.fissure = {
	name = "Fissure",
	asset_name = "fissure",
	category = "Basic Move",
	type = "Rock",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_fissure",
	volume = 0.9
}

-- Rodent
move_stats.swipe = {
	name = "Swipe",
	asset_name = "swipe",
	category = "Basic Move",
	type = "Rodent",
	power = 0.9,
	energy = 2,
	priority = 0,
	animation_frames = 6,
	frame_length = 100,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.gnaw = {
	name = "Gnaw",
	asset_name = "gnaw",
	category = "Basic Move",
	type = "Rodent",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 300,
	sound = "zoonami_bite",
	volume = 1
}

move_stats.bite = {
	name = "Bite",
	asset_name = "bite",
	category = "Basic Move",
	type = "Rodent",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_bite",
	volume = 1
}

move_stats.claw = {
	name = "Claw",
	asset_name = "claw",
	category = "Basic Move",
	type = "Rodent",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 150,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.razor_fang = {
	name = "Razor Fang",
	asset_name = "razor_fang",
	category = "Basic Move",
	type = "Rodent",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 2,
	frame_length = 150,
	sound = "zoonami_razor_fang",
	volume = 0.7
}

-- Spirit
move_stats.sing = {
	name = "Sing",
	asset_name = "sing",
	category = "Basic Move",
	type = "Spirit",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 500,
	sound = "zoonami_sing",
	volume = 0.5
}

move_stats.purify = {
	name = "Purify",
	asset_name = "purify",
	category = "Basic Move",
	type = "Spirit",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 3,
	frame_length = 170,
	sound = "zoonami_purify",
	volume = 0.9
}

move_stats.shine = {
	name = "Shine",
	asset_name = "shine",
	category = "Basic Move",
	type = "Spirit",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 4,
	frame_length = 170,
	sound = "zoonami_shine",
	volume = 0.9
}

move_stats.cleanse = {
	name = "Cleanse",
	asset_name = "cleanse",
	category = "Basic Move",
	type = "Spirit",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 4,
	frame_length = 170,
	sound = "zoonami_cleanse",
	volume = 0.9
}

move_stats.harmony = {
	name = "Harmony",
	asset_name = "harmony",
	category = "Basic Move",
	type = "Spirit",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 200,
	sound = "zoonami_harmony",
	volume = 0.5
}

-- Warrior
move_stats.strike = {
	name = "Strike",
	asset_name = "strike",
	category = "Basic Move",
	type = "Warrior",
	power = 0.90,
	energy = 2,
	priority = 0,
	animation_frames = 5,
	frame_length = 100,
	sound = "zoonami_slash",
	volume = 1
}

move_stats.punch = {
	name = "Punch",
	asset_name = "punch",
	category = "Basic Move",
	type = "Warrior",
	power = 1.0,
	energy = 2,
	priority = 0,
	animation_frames = 6,
	frame_length = 100,
	sound = "zoonami_punch",
	volume = 1
}

move_stats.chop = {
	name = "Chop",
	asset_name = "chop",
	category = "Basic Move",
	type = "Warrior",
	power = 1.1,
	energy = 2,
	priority = 0,
	animation_frames = 2,
	frame_length = 250,
	sound = "zoonami_chop",
	volume = 1
}

move_stats.dropkick = {
	name = "Dropkick",
	asset_name = "dropkick",
	category = "Basic Move",
	type = "Warrior",
	power = 1.6,
	energy = 4,
	priority = 0,
	animation_frames = 3,
	frame_length = 200,
	sound = "zoonami_stomp",
	volume = 1
}

move_stats.sword_swipe = {
	name = "Sword Swipe",
	asset_name = "sword_swipe",
	category = "Basic Move",
	type = "Warrior",
	power = 2.1,
	energy = 6,
	priority = 0,
	animation_frames = 4,
	frame_length = 175,
	sound = "zoonami_slash",
	volume = 1
}

return move_stats
